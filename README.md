# Data

This repository holds the prepared [data set](https://gitlab.hzdr.de/hifis/overall/surveys/survey-2021/data/-/blob/master/data-hifis-survey-2021.csv) of responses to the HIFIS Survey 2021.  
Data preparation included the following steps after manual screening of free text responses:
* Responses of 8 participants were excluded from subset of software development related question: 100, 324, 266, 75, 442, 566, 506, 572
* Responses of 4 participants were excluded from data set due to inconsistent responses: 241, 449, 349, 501
* New categories were added: 
	* `Q010_SQ009`: IT problems / network problems / remote access problems, 
	* `Q010_SQ010`: Operation / maintenance of software services
* New categories were added: 
	* `Q011_SQ010`: Characteristic-based, 
	* `Q011_SQ011`: Collaboration tools / sync and share, 
	* `Q011_SQ012`: Communication tools / conferencing
* New categories were added: 
	* `Q013_SQ010`: Public authorities (national/international), 
	* `Q013_SQ011`: Research institutes/networks
* New categories were added: 
	* `Q014_SQ010`: Ability to be self hosted and interoperability, 
	* `Q014_SQ011`: Information security, 
	* `Q014_SQ012`: Usability/user friendliness`
* New categories were added: 
	* `Q021_SQ038`: Fortran, 
	* `Q021_SQ039`: Labview, 
	* `Q021_SQ040`: Mathematica, 
	* `Q021_SQ041`: VHDL
* New categories were added: 
	* `Q023_SQ002`: C++, 
	* `Q023_SQ003`: Go, 
	* `Q023_SQ004`: Haskell, 
	* `Q023_SQ005`: Java, 
	* `Q023_SQ006`: Julia, 
	* `Q023_SQ007`: Python, 
	* `Q023_SQ008`: R, 
	* `Q023_SQ009`: Rust
* New categories were added: 
	* `Q024_SQ007`: Hands-on experience
* New categories were added:  
    * `Q030/SQ001comment/A001`: Not relevant,
    * `Q030/SQ001comment/A002`: Other priorities,
    * `Q030/SQ001comment/A003`: Lack of time,
    * `Q030/SQ001comment/A004`: Topic or level,
    * `Q030/SQ001comment/A005`: Other networks,
    * `Q030/SQ001comment/A006`: Other
    * `Q030/SQ003comment/A001`: Software Engineering Workshop,
    * `Q030/SQ003comment/A002`: Hacky Hour,
    * `Q030/SQ003comment/A003`: GEOMAR Hacky Hour,
    * `Q030/SQ003comment/A004`: WAW,
    * `Q030/SQ003comment/A005`: Other
    * `Q030/SQ004comment/A001`: Software Engineering Workshop (HIFIS),
    * `Q030/SQ004comment/A002`: Software Engineering Workshop,
    * `Q030/SQ004comment/A003`: External Workshop,
    * `Q030/SQ004comment/A004`: In-house knowledge exchange,
    * `Q030/SQ004comment/A005`: WAW,
    * `Q030/SQ004comment/A006`: Other
* New categories were added: 
	* `Q043_SQ012`: Automation, 
	* `Q043_SQ013`: Data handling software, 
	* `Q043_SQ014`: Data management software
* Raw free text responses were removed after categorization

Details can be found in the [data preparation script](https://gitlab.hzdr.de/hifis/overall/surveys/survey-2021/data/-/blob/master/data-preparation.py) or the [corresponding issue](https://gitlab.hzdr.de/hifis/overall/surveys/survey-2021/data/-/issues/2).  
For a description of the full survey, please have a look at the [meta data](https://gitlab.hzdr.de/hifis/overall/surveys/survey-2021/analysis/-/tree/master/metadata).
