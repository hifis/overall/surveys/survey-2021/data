import re
from typing import Dict, List, Set

from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from numpy import nan
from pandas import DataFrame, Series, unique, read_csv

from descriptive_stats import generate_word_cloud


def _load_data(path: str) -> DataFrame:
    """Load data from LimeSurvey export with the following settings:
        - as CSV file
        - question and answer codes
        - use Expression Manager"""
    # Load raw data
    data_df: DataFrame = read_csv(filepath_or_buffer=path, sep=",")
    # Set LimeSurvey IDs as index
    data_df.set_index(keys="id", drop=True, inplace=True)
    data_df.loc[:, "Q007"] = data_df.loc[:, "Q007"].astype("Int32")

    return data_df


def _fix_typos(data_df: DataFrame) -> DataFrame:
    """"Fix typos found through manual screening"""
    data_df.replace(
        to_replace={
            "phyton": "python",
            "Phyton": "Python",
            "Juia": "Julia",
            "besteimmte": "bestimmte",
            '"': "'",
            "„": "'",
            "“": "'",
            "https.*": "",
        },
        regex=True,
        inplace=True
    )

    return data_df


def _remove_invalid_answers(data_df: DataFrame) -> DataFrame:
    """Remove invalid answers"""
    # Remove software related in answers in cases in which respondents stated
    # that they were not developing software and hence could not answer the
    # respective questions properly
    non_developers: Set = {75, 100, 266, 324, 442, 506, 566, 572}
    questions: List = [
        question for question in data_df.columns if re.match(
            r"^Q020|Q021|Q022|Q025|Q026|Q027|Q028|Q029|Q038|Q039|Q040|Q041|" +
            "Q042|Q043|Q044", question
        )
    ]
    data_df.loc[non_developers, questions] = None
    print("\nResponses of {} participants were excluded from subset of "
          "software development related question: {}"
          .format(len(non_developers), non_developers))

    # Remove all answers in cases in which respondents gave inconsistent
    # responses because this could be simply try-runs
    has_developed_last_year: Set = \
        set(data_df[data_df["Q005"] == "A002"].index)
    does_not_develop_sw: Set = \
        set(data_df[data_df["Q006"] == "A006"].index)
    inconsistent_responses = \
        has_developed_last_year.intersection(does_not_develop_sw)
    data_df.loc[inconsistent_responses, :] = None
    print("\nResponses of {} participants were excluded from data set due to "
          "inconsistent responses: {}"
          .format(len(inconsistent_responses), inconsistent_responses))

    return data_df


def _prepare_cloud_word_cloud(data_df: DataFrame) -> None:
    """Generate a word cloud plot from prepared free text responses"""
    # Relevant cloud services questions
    free_text_fields: List = ["Q010_other", "Q011_other", "Q013_other",
                              "Q014_other"]

    # Remove stop words and lemmatize free text
    free_text: str = _normalize_free_text(data_df=data_df,
                                          free_text_fields=free_text_fields)

    # Plots answers as word cloud
    generate_word_cloud(free_text_response=free_text,
                        question="Cloud")


def _prepare_q001(data_df: DataFrame) -> DataFrame:
    """Categorize affiliation mentioned in free text answers keeping "Other"
    as default category."""
    q001_oth_map: Dict = {
        "Baumanagement im DLR": "A005",
        "DLR": "A005",
        "Helmholtz Auslandbüro": "A011",
        "HMC im GFZ": "A009"
    }

    data_df.loc[:, "Q001_other"].replace(to_replace=q001_oth_map, inplace=True)
    data_df.loc[:, "Q001"].update(data_df.loc[:, "Q001_other"])
    data_df.loc[:, "Q001_other"] = None

    return data_df


def _prepare_q010(data_df: DataFrame) -> DataFrame:
    """Categorize time-consuming work-flows mentioned in free text field.
    If workflow is mentioned less than 3 times assign "Other" as default
    category."""
    data_df.loc[:, "Q010_other"].replace(
        to_replace={
            "(1) Aufarbeitung von Datenverlust bei Updates von nubes, "
            "(2) Kuratierung und Synchronisation neuerer mit älteren Dateien "
            "(z.B. nubes)":
                "other",
            "?":
                None,
            "Administration":
                "other",
            "Anpassung / Installation von Software auf neuem Rechencluster "
            "(Längere Lebenszeit von Rechenclustern wünschenswert)":
                "Operation/maintenance of software services",
            "Archivierung nach Vorlagen (z.B. von Pangaea)":
                "other",
            "Bearbeitung von forschungsdaten":
                "other",
            "Betreuung der produktiven Software":
                "Operation/maintenance of software services",
            "Bin Laborarbeiterin":
                None,
            "Einarbeitung in schlecht Dokumentierten Code und Debugging":
                "Operation/maintenance of software services",
            "Einarbeitung in unkommentierten Code und Debugging":
                "Operation/maintenance of software services",
            "Einrichten neuer Softwareservices fürs Institut":
                "Operation/maintenance of software services",
            "Entwicklung eigener Software":
                "other",
            "Eruieren bestehender Möglichkeiten und Strukturen":
                "other",
            "IT Probleme ":
                "IT problems/network problems/remote access problems",
            "kein Bezug zur Frage":
                None,
            "mache ich nicht":
                None,
            "n.n.":
                None,
            "Netzwerkprobleme, Dateizugriff":
                "IT problems/network problems/remote access problems",
            "neue Software schreiben, ergänzen":
                "other",
            "normale Nutzer IT Pobleme":
                "IT problems/network problems/remote access problems",
            "Organisation und Aufrechterhaltung von Remote-Zugängen":
                "IT problems/network problems/remote access problems",
            "Personalgewinnung":
                "other",
            "Simulation period":
                "other",
            "Suche nach erfahrenen und geeigneten Ansprechpartnern":
                "other",
            "Testen der entwickelten Software":
                "other",
            "trifft nichts zu":
                None,
            "Überzeugen von Kolleg:innen die vorhanden Werkzeuge zu nutzen":
                "other",
            "Vermittlung von Grundlagen des Umgangs mit Daten":
                "other",
            "working remotely, the connection to the central DZNE server is "
            "often faulty. even using VPN or remote desktop, it is difficult "
            "to store, update, access my own folder on the central server. "
            "ideally, all files on my laptop should automatically synchronize "
            "with the central folder, as in other institutes of the kind "
            "(at least, abroad). this is not only a problem in corona times, "
            "as researchers often have to travel for congresses or "
            "collaborations, or need to work on different locations on their "
            "own laptops. ":
                "IT problems/network problems/remote access problems"
        },
        inplace=True
    )

    # Mark sub-question as being checked where described aspect fits in one of
    # the existing categories
    data_df = _check_existing_sub_questions(data_df=data_df, question="Q010")

    # Add new categories as additional sub-questions
    data_df = _construct_new_categories(data_df=data_df, question="Q010")

    return data_df


def _prepare_q011(data_df: DataFrame) -> DataFrame:
    """Categorize cloud services that should be replaced mentioned in free text
    field. If cloud service is mentioned less than 3 times assign "Other" as
    default category."""
    data_df.loc[:, "Q011_other"].replace(
        to_replace={
            "Alle genannten (bis auf MS Office/Teams) "
            "würde ich gerne nutzen/nutze ich gern":
                "Communication tools/conferencing",
            "alles, was nicht open source ist! ":
                "Characteristic-based",
            "Atlassian-Produkte: Jira":
                "Characteristic-based",
            "benutze keine Clouddienste":
                None,
            "Brauchen erstmal einen ":
                None,
            "Bzw. gute interne Alternativen zu Lösungen wie Google Docs und "
            "Dropbox mit ähnlicher Funktionalität und Möglichkeiten der "
            "einfachen kollaborativen Zusammenarbeit.":
                "Collaboration tools/Sync & Share",
            "confluence, desy sync-and-share":
                "Collaboration tools/Sync & Share",
            "Confluence, Jira, Bitbucket":
                "Characteristic-based",
            "currently, i have a sort of synchronization of my files with "
            "NextCloud. that does not need to be replaced, however such "
            "synchronization should ideally be somehow allowed on the central "
            "DZNE server if possible. or, at least, folder updates should be "
            "made possible (connection is too slow and normally aborts)":
                "Collaboration tools/Sync & Share",
            "desycloud, cerncloud":
                "Collaboration tools/Sync & Share",
            "ersetzen mit was???":
                None,
            "ersetzen? mit was?":
                None,
            "Europäischen Dienst (kein US-Dienst!) mit Server in Deutschland":
                "Characteristic-based",
            "Europäischen Dienst mit Server in Deutschland":
                "Characteristic-based",
            "externen dedicated Server":
                "other",
            "(Frage war unklar, einsetzen oder ersetzen? "
            "Ich nutze GitHub, Zoom, Jitsi, Matrix, und ich würde die "
            "folgenden Services gerne nicht nutzen müssen "
            "(muss aber, da von anderen verwendet): "
            "Skype, MS teams, Slack":
                "Communication tools/conferencing",
            "GitLab":
                "Characteristic-based",
            "Gitlab, Bitbucket":
                "Characteristic-based",
            "GoTo":
                "Communication tools/conferencing",
            "kann ich nicht beantworten":
                None,
            "Kubernetes or application hosting":
                "other",
            "NextCloud and/or OnlyOffice, OO doesn't work reliable for collab "
            "since years":
                "Collaboration tools/Sync & Share",
            "Nextcloud":
                "Collaboration tools/Sync & Share",
            "nextcloud":
                "Collaboration tools/Sync & Share",
            "Nextcloud, Kubernetes, Collabora":
                "Collaboration tools/Sync & Share",
            "On Premise Lösungen":
                "Characteristic-based",
            "Overleaf":
                "other",
            "own cloud":
                "Collaboration tools/Sync & Share",
            "sharepoint":
                "Collaboration tools/Sync & Share",
            "von DFN o.ä. bereitgestellte":
                "Characteristic-based",
            "webex":
                "Communication tools/conferencing",
            "Webex, Projektmanagementtool (z.B. Trello, Asana..)":
                "Communication tools/conferencing",
            "Wir benutzen keine Cloud-Dienste":
                None
        },
        inplace=True
    )

    # Mark sub-question as being checked where described aspect fits in one of
    # the existing categories
    data_df = _check_existing_sub_questions(data_df=data_df, question="Q011")

    # Add new categories as additional sub-questions
    data_df = _construct_new_categories(data_df=data_df, question="Q011")

    return data_df


def _prepare_q013(data_df: DataFrame) -> DataFrame:
    """Categorize collaboration partners mentioned in free text field.
    If category is mentioned less than 3 times assign "Other" as default
    category."""
    data_df.loc[:, "Q013_other"].replace(
        to_replace={
            "Agenturen, freie Autoren":
                "other",
            "BMBF":
                "Public authorities",
            "Bundesanstalt BGR":
                "Public authorities",
            "CERN":
                "Research institutes/networks",
            "DZGs (z.B. DZIF, DZD)":
                "Research institutes/networks",
            "Deutsche und Internationale Behoerden, NGOs":
                "Public authorities",
            "Deutschen Behörden":
                "Public authorities",
            "EU Projektpartner":
                "other",
            "GWDG":
                "Research institutes/networks",
            "Internationale Forschungsdatennetzwerke und -Gremien":
                "Research institutes/networks",
            "Landesbehörde":
                "Public authorities",
            "Landratsämter  SAB":
                "Public authorities",
            "NGOs":
                "other",
            "öffentliche Einrichtungen/Behörden (Deutschland, EU)":
                "Public authorities",
            "Planungsbüros":
                "other",
            "Ufu.e.V.":
                "Research institutes/networks"
        },
        inplace=True
    )
    # Mark sub-question as being checked where described aspect fits in one of
    # the existing categories
    data_df = _check_existing_sub_questions(data_df=data_df, question="Q013")

    # Add new categories as additional sub-questions
    data_df = _construct_new_categories(data_df=data_df, question="Q013")

    return data_df


def _prepare_q014(data_df: DataFrame) -> DataFrame:
    """Categorize criteria for choosing a cloud service mentioned in free text
    field. If criterion is mentioned less than 3 times assign "Other" as
    default category."""
    data_df.loc[:, "Q014_other"].replace(
        to_replace={
            ".":
                None,
            "Ability to control access, while still collaborating with "
            "'external' people":
                "Information security",
            "benötige ich nciht":
                None,
            "Benutzerfreundlichkeit":
                "Usability/User Friendliness",
            "bisher habe ich nie cloud dienste selbst gewählt. "
            "die it abteilung ist dafür zuständig":
                None,
            "dass jeder den client installieren darf":
                "Information security",
            "Daten verbleiben innerhalb der eigenen Einrichtung":
                "Ability to be self hosted & interoperability",
            "Daten verschlüsselt":
                "Information security",
            "Eigenständige Administration":
                "Ability to be self hosted & interoperability",
            "einfach zu nutzen":
                "Usability/User Friendliness",
            "Funktionalität":
                "other",
            "ich wähle nicht selber, sondern es wird mir vorgegeben, "
            "welche Dienste ich nutzen kann":
                None,
            "Interoperabilität":
                "Ability to be self hosted & interoperability",
            "kann ich fachlich nicht beurteilen":
                None,
            "keine Ahnung":
                None,
            "Nutzbarkeit":
                "Usability/User Friendliness",
            "Nutzung unter Linux":
                "Ability to be self hosted & interoperability",
            "Selfhosted":
                "Ability to be self hosted & interoperability",
            "Sicherheitsbedenken unseres Kunden verhindern die Anwendung von "
            "Cloud-Diensten":
                "Information Security",
            "trifft nicht zu":
                None,
            "Usability":
                "Usability/User Friendliness",
            "usability":
                "Usability/User Friendliness",
            "User Friendliness (z.B. Google Drive)":
                "Usability/User Friendliness",
            "vendor lock-in":
                "Ability to be self hosted & interoperability",
            "von meiner Einrichtung unterstützt/freigegeben":
                "Information Security",
            "Wir benutzen meines Wissens keine Cloud Dienste":
                None,
            "Wir verwenden keine Cloud-Dienste.":
                None
        },
        inplace=True
    )
    # Mark sub-question as being checked where described aspect fits in one of
    # the existing categories
    data_df = _check_existing_sub_questions(data_df=data_df, question="Q014")

    # Add new categories as additional sub-questions
    data_df = _construct_new_categories(data_df=data_df, question="Q014")

    return data_df


def _prepare_q021(data_df: DataFrame) -> DataFrame:
    """Categorize programming languages mentioned in free text field.
    If programming language is mentioned less than 3 times assign "Other" as
    default category."""
    # Add new categories as additional sub-questions
    data_df = _construct_new_categories(data_df=data_df, question="Q021")

    # Summarize remaining answers as "Other" category
    data_df.loc[:, "Q021_other"].replace(
        to_replace={
            "AutoIt": "Y",
            "html": "Y",
            "keine davon": None,
            "Labtalk": "Y",
            "Modelica": "Y",
            "OPUS": "Y",
            "Oracle*Forms": "Y",
            "Purescript": "Y",
            "REXX": "Y",
            "Scheme": "Y",
            "SNL State Notation Language": "Y",
            "stan": "Y",
            "Stata": "Y"
        },
        inplace=True)

    return data_df


def _prepare_q022(data_df: DataFrame) -> DataFrame:
    """Fix incorrect LimeSurvey answer codes."""
    data_df.loc[:, "Q022"] = data_df.loc[:, "Q022"].str.replace(
        pat="SQ",
        repl="A"
    )
    data_df.loc[188, "Q022"] = "stan"  # information from free text response

    return data_df


def _prepare_q023(data_df: DataFrame) -> DataFrame:
    """Categorize programming languages mentioned in free text field.
    If programming language is mentioned less than 3 times assign "Other" as
    default category."""
    # Set explicit "Yes" answer
    data_df.loc[:, "Q023"].replace(to_replace={"-oth-": "A002"}, inplace=True)

    # Add new categories as additional sub-questions
    data_df = _construct_new_categories(data_df=data_df, question="Q023")

    # Summarize remaining answers as "Other" category
    data_df.loc[:, "Q023_other"].replace(
        to_replace={
            "ABAP": "Y",
            "C": "Y",
            "Clojure": "Y",
            "Einsatz von JSON-LD": "Y",
            "FORTRAN": "Y",
            "LabView": "Y",
            "OpenCL": "Y",
            "PHP": "Y",
            "react, Nod.js": "Y",
            "VBA": "Y",
            "Verilog": "Y",
            "VHDL": "Y",
        },
        inplace=True
    )

    return data_df


def _prepare_q024(data_df: DataFrame) -> DataFrame:
    """Categorize forms of learning mentioned in free text field.
    If a particular form of learning is mentioned less than 3 times assign
    "Other" as default category."""
    data_df.loc[:, "Q024_other"].replace(
        to_replace={
            "Austausch mit erfahrenen Kollegen":
                "Knowledge exchange",
            "Coaching ":
                "SQ004",
            "Discord":
                "Knowledge exchange",
            "eigenständiges lernen durch Interesse":
                "SQ003",
            "Kurse":
                "SQ001",
            "Learning by doing":
                "Hands-on experience",
            "platforms like Udemy":
                "SQ004",
            "Praktische Anwendung mit aktuellem Bezug":
                "Hands-on experience",
            "selfpaced e-learning course":
                "SQ004"
        },
        inplace=True
    )

    # Mark sub-question as being checked where described aspect fits in one of
    # the existing categories
    data_df = _check_existing_sub_questions(data_df=data_df, question="Q024")

    # Add new categories as additional sub-questions
    data_df = _construct_new_categories(data_df=data_df, question="Q024")

    # Summarize remaining answers as "Other" category
    data_df.loc[:, "Q024_other"].replace(
        to_replace={
            "Knowledge exchange": "Y",
        },
        inplace=True
    )

    return data_df


def _prepare_q025(data_df: DataFrame) -> DataFrame:
    """Categorize key challenges mentioned in free text field.
    If a particular key challenge is mentioned less than 3 times assign
    "Other" as default category."""
    # Summarize free answers as "Other" category
    data_df.loc[:, "Q025_other"].replace(
        to_replace={
            "bessere numerische Performance von Solvern/Algorithmen":
                "other",
            "Das komplette Fehlen jeglicher Unterstützung. "
            "Ein bedeutendes Projekt musste ich aufgeben, "
            "es ist aktiv 'ausgehungert' worden.":
                "other",
            "Data Privacy / Access Control":
                "other",
            "Debugging":
                "other",
            "GDPR compliant and secure SCM and CI":
                "other",
            "handover von partiellem code bei Personalwechsel":
                "SQ007",
            "Laufende Änderung der Anforderungen im Coding-Prozess":
                "SQ003",
            "mangelnde Kenntnisse":
                "other",
            "ohne 'die programmieren können'":
                "SQ011",
            "risk assessment":
                "other",
            "Schreiben von Analyseprogrammen großer Datenmengen "
            "(RNAseq, Single Cell) in R/Perl/Python und Bash.":
                "other",
            "solche Projekte habe ich nicht":
                None,
            "verwenden automatisierter Auswertungsprogramme für die "
            "Histologie ohne Vorerfahrung in der Programmierung":
                "other",
            "Wie man einen sauberen Code anfängt, organisiert, kontrolliert":
                "SQ001, SQ006"
        },
        inplace=True
    )

    # Mark sub-question as being checked where described aspect fits in one of
    # the existing categories
    data_df = _check_existing_sub_questions(data_df=data_df, question="Q025")

    return data_df


def _prepare_q026(data_df: DataFrame) -> DataFrame:
    """Categorize approaches to solve challenges mentioned in free text field.
    If a particular approach is mentioned less than 3 times assign
    "Other" as default category."""
    data_df.loc[:, "Q026_other"].replace(
        to_replace={
            "Sturheit":
                "other",
            "Bessere Selbstorganisation um effizienter zu arbeiten":
                "other",
            "Hilfe der IT Abtl.":
                "other",
            "Schulung der Infrastrukturabteilung":
                "other",
            "viel Kommunikation und Koordination":
                "other"
        },
        inplace=True
    )

    # Mark sub-question as being checked where described aspect fits in one of
    # the existing categories
    data_df = _check_existing_sub_questions(data_df=data_df, question="Q026")

    return data_df


def _prepare_q030_sq001(data_df: DataFrame) -> DataFrame:
    """Categorize reasons for not participating in any community event"""
    data_df.loc[:, "Q030_SQ001comment"].replace(
        to_replace={
            ".":
                None,
            "Andere Fachgebiete meiner Arbeit relevanter sind":
                "A002",
            "Code schreiben und Softwareentwicklung für meine "
            "Arbeit nicht nötig ist":
                "A001",
            "Corona":
                "A006",
            "Es für meine Tätigkeit nicht relevant ist ":
                "A001",
            "Es ist nicht mein Aufgabenschwerpunkt an der Geschäftsstelle":
                "A002",
            "Gehört nicht zu meinen Kernaufgaben":
                "A001",
            "Ich mich noch in einer Orientierungsphase befinde":
                "A002",
            "Ich nicht programmiere sondern anwende.":
                "A001",
            "Keine Zeit":
                "A003",
            "Kosten(Zeit)/Nutzen-Ratio ungünstig":
                "A003",
            "Nicht mein Themengebiet":
                "A001",
            "Softwareentwicklung ist nicht Teil meiner beruflichen Aufgaben":
                "A001",
            "Softwareentwicklung nicht mein Schwerpunkt ist":
                "A002",
            "Softwareentwicklung nicht mein Schwerpunkt ist.":
                "A002",
            "Softwareentwicklung nicht meine Hauptaufgabe ist und "
            "zeitlich schwer unterzubringen.":
                "A002",
            "Softwarentwicklung ist nicht meine wesenliche Aufgabe":
                "A002",
            "Zeitbudget ausgeschöpft":
                "A003",
            "Zeitmangel":
                "A003",
            "bisher bei mir kein ausreichendes Vorwissen zu den behandelten "
            "Themen vorhanden ist":
                "A004",
            "busy with other work.":
                "A002",
            "dafür die Zeit fehlt. Meine KernAufgabe ist die Projektleitung "
            "wissenschaftlicher Fragenstellungen":
                "A002",
            "das Thema nicht relevant für meine Arbeit ist":
                "A001",
            "das nichts mit meiner Arbeit zu tun hat":
                "A001",
            "das nur 10% meiner ohnehin knappen Arbeitszeit umfasst.":
                "A003",
            "derzeit kein Bedarf":
                "A001",
            "die Themen irrelevant waren":
                "A004",
            "die Veranstaltungen sich an eine andere Zielgruppe richten":
                "A004",
            "entweder das Thema nicht relevant war oder nicht für meine "
            "Erfahrungsstufe passte.":
                "A004",
            "es für meine Arbeit nicht relevant ist":
                "A001",
            "gesamtprojektleitung, das ist nur ein Aspekt, wuerde auf einem "
            "solchen Meeting zuviel 'experten-Latein' erwarten":
                "A004",
            "ich Spezialfragen via Tutorials und Suchmaschinen gewöhnlich "
            "schneller beantworten kann.":
                "A005",
            "ich bin nicht mehr aktiv, bin im Ruhestand, arbeite nur noch aus "
            "Interesse":
                "A002",
            "ich dafür keine Zeit aufwenden kann. ":
                "A003",
            "ich das auf diesen Workshops typischerweise vermittelte Wissen "
            "bereits habe":
                "A004",
            "ich das bisher nicht benötigt habe.":
                "A001",
            "ich das nötige Wissen und meine eigenen Netzwerke in meinem "
            "Zentrum habe.":
                "A005",
            "ich erst mal Basics lernen müsste und es kein Schwerpunkt meines "
            "Arbeitsalltags ist":
                "A002",
            "ich es nicht benötige.":
                "A001",
            "ich genug guten Austausch mit den Kolleg:innen habe.":
                "A005",
            "ich habe alle Voraussetzungen darin, um die Aufgaben zu "
            "bewältigen. ":
                "A001",
            "ich im Moment Online Veranstaltungen als zu anstrengend empfinde":
                "A006",
            "ich keine Interesse habe.":
                "A001",
            "ich keine Kerkompetenz in der Softwareentwicklung habe":
                "A001",
            "ich keine Software entwickele.":
                "A001",
            "ich keine Software entwickeln muss":
                "A001",
            "ich keine Zeit dafür gehabt habe. ":
                "A003",
            "ich keinen Code schreibe":
                "A001",
            "ich nicht mehr programmiere.":
                "A001",
            "ich nicht programmiere":
                "A001",
            "ich sehr wenig programmiere":
                "A001",
            "ich zu wenig Code schreibe in meiner alltäglichen Arbeit":
                "A002",
            "ich zur zeit nicht programmiere":
                "A001",
            "in Elternzeit":
                None,
            "it is not related to my project ":
                "A001",
            "keine Verwendung dafür":
                "A001",
            "keine Zeit":
                "A003",
            "keine Zeit, Projekte nicht groß genug, "
            "als das es sich lohnen würde.":
                "A003",
            "keine Zeit; Softwareentwicklung in der Wissenschaft wird zu "
            "gering geschätzt":
                "A003",
            "keine derartigen Arbeiten geplant sind.":
                "A001",
            "meine Lern- und Arbeitsweise nicht zu der heterogenen "
            "Teilnehmer-Zusammensetzung in solchen Veranstaltungen passt":
                "A004",
            "meine Projekte bisher zu klein waren, das es sich lohnen würde":
                "A001",
            "meine Projekte zu klein sind und meist nur mir selbst etwas "
            "nuetzen":
                "A001",
            "my field of work is desiging aircraft cabins":
                "A001",
            "nicht Mei eigentlicher Tätigkeitsbereich":
                "A002",
            "nicht relevant für meine Arbeit":
                "A001",
            "sehr spezielle Kombination an Programmen und Datenbanken":
                "A004",
            "they are usually noise and not action":
                "A006",
            "wenig Zeit":
                "A003",
            "zeitintensiv, besser online meetings und befrage mein "
            "persönliches netzwerk":
                "A005",
            "zu sehr auf Anfänger fokussiert":
                "A004"
        },
        inplace=True
    )
    print("New categories: {'Q030/SQ001comment/A001': 'Not relevant',"
          "'Q030/SQ001comment/A002': 'Other priorities',"
          "'Q030/SQ001comment/A003': 'Lack of time',"
          "'Q030/SQ001comment/A004': 'Topic or level',"
          "'Q030/SQ001comment/A005': 'Other networks',"
          "'Q030/SQ001comment/A006': 'Other'}")

    return data_df


def _prepare_q030_sq002(data_df: DataFrame) -> DataFrame:
    """Prepare mentions of topics that were not addressed in community events
    for wordcloud generation"""
    # Relevant questions
    free_text_fields: List = ["Q030_SQ002comment"]

    # Remove stop words and lemmatize free text
    free_text: str = _normalize_free_text(data_df=data_df,
                                          free_text_fields=free_text_fields)

    # Plot responses as word cloud
    generate_word_cloud(free_text_response=free_text,
                        question=free_text_fields[0])

    # Remove free text responses from data set
    data_df.loc[:, free_text_fields] = None

    return data_df


def _prepare_q030_sq003(data_df: DataFrame) -> DataFrame:
    """Categorize events that people have heard of but not visited"""
    data_df.loc[:, "Q030_SQ003comment"].replace(
        to_replace={
            " Introduction to Python / 02.02.2021 - 11.02.2021":
                "A001",
            "Carpentries":
                "A001",
            "DLR Wissensaustauschworkshops zu Softwareentwicklung und zu KI":
                "A004",
            "GEOMAR Hacky Hour":
                "A003",
            "Geomar Hacky Hour":
                "A003",
            "HIP networking/project event":
                "A005",
            "Hacky Hour":
                "A002",
            "Hacky Hours":
                "A002",
            "Hacky hour":
                "A002",
            "Hackyhour":
                "A002",
            "Helmholtz workshops":
                "A001",
            "Introduction to Git (HIFIS)":
                "A001",
            "Policies für Forschungssoftware":
                "A005",
            "Python Workshop":
                "A001",
            "Python hacky-hour at GEOMAR":
                "A003",
            "Wissensaustausch Workshop DLR":
                "A004",
            "project management in GitLab (organised by HIFIS ;) )":
                "A001",
            "software carpentry workshop":
                "A001"
        },
        inplace=True
    )
    print("New categories: {'Q030/SQ003comment/A001': "
          "'Software Engineering Workshop',"
          "'Q030/SQ003comment/A002': 'Hacky Hour',"
          "'Q030/SQ003comment/A003': 'GEOMAR Hacky Hour',"
          "'Q030/SQ003comment/A004': 'WAW',"
          "'Q030/SQ003comment/A005': 'Other'}")
    return data_df


def _prepare_q030_sq004(data_df: DataFrame) -> DataFrame:
    """Categorize visited events"""
    data_df.loc[:, "Q030_SQ004comment"].replace(
        to_replace={
            "Bring Your Own Script and Make It Ready for Publication":
                "A001",
            "Containers for Scientific Computing":
                "A002",
            "Digital kitchen HZDR":
                "A004",
            "Einführung Gitlab":
                "A001",
            "Einführung git":
                "A001",
            "FHIR Tutorial":
                "A003",
            "Git + GitLab":
                "A001",
            "Gitlab":
                "A001",
            "Gitlab for Administrative Staff":
                "A002",
            "HEPiX, CHEP":
                "A003",
            "HIDA":
                "A005",
            "HIFIS Gitlab workshop":
                "A001",
            "Helmholtz AI Roadshow":
                "A005",
            "Image Analysis am HZDR":
                "A004",
            "Introduction to Gitlab":
                "A001",
            "MarData Blockcourse":
                "A005",
            "OpenACC":
                "A003",
            "PX4 Developer Conference":
                "A003",
            "Python Einsteigerkurs":
                "A002",
            "Python and Git course from hifis":
                "A001",
            "Python crash course":
                "A002",
            "SoRSE Vortäge":
                "A003",
            "Software Policy Workshop":
                "A002",
            "Software carpentry by HIFIS, introduction to pyton by UFZ":
                "A001",
            "Softwareentwicklung für Wissenschaftler "
            "(DLR interne Fortbildung)":
                "A001",
            "Themen-Workshop von professionellem Schulungs-Anbieter":
                "A003",
            "Veranstaltungen im HIFIS Kontext":
                "A005",
            "Version Control using GIT":
                "A001",
            "WaW Machine Learning":
                "A004",
            "Workshop zu HPC":
                "A002",
            "betriebsinterner HoloLens-Wissensaustausch":
                "A004",
            "climate system department":
                "A005",
            "cpp":
                "A005",
            "hausintern zu LabView":
                "A004",
            "virtual software carpentry workshop (git etc.) "
            "from 22-23June, 2020 GFZ":
                "A001"
        },
        inplace=True
    )
    print("New categories: {'Q030/SQ004comment/A001': "
          "'Software Engineering Workshop (HIFIS)',"
          "'Q030/SQ004comment/A002': "
          "'Software Engineering Workshop',"
          "'Q030/SQ004comment/A003': 'External workshop',"
          "'Q030/SQ004comment/A004': "
          "'In-house knowledge exchange',"
          "'Q030/SQ004comment/A005': 'WAW',"
          "'Q030/SQ004comment/A006': 'Other'}")

    return data_df


def _prepare_q030_sq005(data_df: DataFrame) -> DataFrame:
    """Categorize organized events"""
    data_df.loc[:, "Q030_SQ005comment"].replace(
        to_replace={
            "Hackathon mit der Projektgruppe":
                "Other"
        },
        inplace=True
    )

    return data_df


def _prepare_q031(data_df: DataFrame) -> DataFrame:
    """Mark "No answer" option as missing value."""
    data_df.loc[:, ["Q031_SQ001", "Q031_SQ002"]] = \
        data_df.loc[:, ["Q031_SQ001", "Q031_SQ002"]].replace(
            to_replace="A000",
            value=None
        )

    return data_df


def _prepare_q033(data_df: DataFrame) -> DataFrame:
    """Categorize forms of participation mentioned in free text field.
    If a particular form of participation is mentioned less than 3 times assign
    "Other" as default category."""
    data_df.loc[:, "Q033_other"].replace(
        to_replace={
            "attending a workshop":
                "Attending a workshop",
            "konkrete loseungsorientierter Workshop":
                "Attending a workshop",
            "onlien zu hören, mitmachen":
                None,
            "tutorials":
                None,
            "   ":
                None
        },
        inplace=True
    )

    # Mark sub-question as being checked where described aspect fits in one of
    # the existing categories
    data_df = _check_existing_sub_questions(data_df=data_df, question="Q033")

    # Add new categories as additional sub-questions
    data_df = _construct_new_categories(data_df=data_df, question="Q033")

    # Summarize remaining answers as "Other" category
    data_df.loc[:, "Q033_other"].replace(
        to_replace={
            "Attending a workshop": "Y"
        },
        inplace=True
    )

    return data_df


def _prepare_q034(data_df: DataFrame) -> DataFrame:
    """Prepare mentions of topics that are of interest for community events
    for wordcloud generation"""
    # Get all free text fields of this question
    free_text_fields: List = [
        sub_question for sub_question in data_df.columns.values if
        sub_question.startswith("Q034")
    ]

    # Remove stop words and lemmatize free text
    free_text: str = _normalize_free_text(data_df=data_df,
                                          free_text_fields=free_text_fields)

    # Plot free text as word cloud
    generate_word_cloud(free_text_response=free_text,
                        question="Q034")

    # Remove free text from data set
    data_df.loc[:, free_text_fields] = None

    return data_df


def _prepare_q037(data_df: DataFrame) -> DataFrame:
    """Categorize communication channels mentioned in free text field.
    If a particular communication channel is mentioned less than 3 times assign
    "Other" as default category."""
    data_df.replace(
        to_replace={
            "email":
                "SQ002",
            "Sektionsleitung":
                "other",
            "Mattermost":
                "other",
            "die guten alten Printmedien":
                "other",
            "Helmholtz- Infoveranstaltung":
                "other",
            "Podcast":
                "other",
            "not sure i would get the potential of what is offered. "
            "what i need is that when i have a need, the IT service can "
            "provide me a suitable service. as said, my needs are very basic, "
            "but even those are not really answered sufficiently. "
            "thanks for your help!":
                None,
            "gebuendelte info-mails ueber Zentren":
                "SQ002",
            "UFZ WKDV":
                "other"
        },
        inplace=True
    )

    # Mark sub-question as being checked where described aspect fits in one of
    # the existing categories
    data_df = _check_existing_sub_questions(data_df=data_df, question="Q037")

    return data_df


def _prepare_q038(data_df: DataFrame) -> DataFrame:
    """Extract team size from free text answers. If more than one number is
    given choose maximum. Assign None to all other comments."""
    # Update value where free text answer suggests a different answer category
    data_df.loc[271, "Q038"] = "A002"
    data_df.loc[490, "Q038"] = "A002"

    # Assign team sizes as extracted from free text
    cases: List = data_df["Q038_comment"].dropna().index
    values = [
        None,
        5,
        3,
        None,
        None,
        3,
        4,
        None,
        None,
        None,
        None,
        None,
        None,
        3,
        2,
        None,
        None,
        2,
        15,
        3,
        4,
        3,
        3,
        None,
        6,
        1,
        None,
        2,
        None,
        None,
        6,
        None,
        20,
        4,
        1,
        1,
        3,
        5,
        None,
        20,
        10,
        5,
        None,
        1,
        1,
        1,
        2,
        None,
        None,
        None,
        1,
        1,
        10,
        None,
        1,
        1
    ]
    team_sizes: Dict = dict(zip(cases, values))
    for case, value in team_sizes.items():
        data_df.loc[case, "Q038_comment"] = value

    # Set team size to 1 where category "One developer" was chosen
    data_df.loc[:, "Q038_comment"].mask(
        data_df.loc[:, "Q038"] == "A001", 1, inplace=True
    )
    return data_df


def _prepare_q040(data_df: DataFrame) -> DataFrame:
    """Fix incorrect LimeSurvey sub-question codes."""
    data_df.rename(
        columns={"Q040_SQ002": "Q040_SQ003", "Q040_SQ003": "Q040_SQ002"},
        inplace=True
    )
    return data_df


def _prepare_q043(data_df: DataFrame) -> DataFrame:
    """Categorize software types mentioned in free text field. If software
    type is mentioned less than 3 times assign "Other" as default category."""
    data_df.loc[:, "Q043_other"].replace(
        to_replace={
            "Automatisierte Gefahrenstofferkennung":
                "other",
            "Automatisierung":
                "Automation",
            "Benchmarking":
                "Automation",
            "Data acquisition system":
                "Data handling software",
            "Data analysis software (not 'scripts'!)":
                "Data handling software",
            "Data processing software for data archiving":
                "Data handling software",
            "Datenanalyse":
                "Data handling software",
            "Datenerfassungsskript in automatisierten Messsystem":
                "Data handling software",
            "Datenintegration":
                "Data handling software",
            "Datenmanagement":
                "Data management software",
            "Diese Frage trifft auf mich NICHT zu: "
            "Ich entwickle keine Software - "
            "muss hier allerdings etwas ankreuzen...":
                None,
            "Echtzeit-Datenverarbeitung in FPGA":
                "Data handling software",
            "Experimentsteuerung":
                "SQ009",
            "FPGA-Code":
                None,
            "Flugsoftware Raumfahrt":
                "SQ005",
            "Forschungsdatenmanagement":
                "Data management software",
            "Gar nichts":
                None,
            "Komplexe Teststandsteuerung und Sensorik":
                "SQ009",
            "LabView, Steuereung eines komplexen Messstandes":
                "SQ009",
            "Mathematica-codes":
                None,
            "Praktische Umsetzunge eines Protokolls zu Demonstrations- und "
            "Benchmark-Zwecken":
                "SQ009",
            "Realtime-Datenverarbeitung in einer FPGA":
                "Data handling software",
            "Roboterprogrammierung":
                "SQ004",
            "Scala":
                None,
            "Siehe Kommentar oben - not applicable":
                None,
            "Skripte zur Automatisierung von Prozessen":
                "Automation",
            "Verwaltungssoftware":
                "Data management software",
            "Wiki-Datenbank ":
                "SQ003",
            "externe Geräte auslesen, und auswerten":
                "Data handling software",
            "kein Ahnung":
                None,
            "kein code":
                None,
            "keine Software entwickelt":
                None,
            "nicht direkt IN der Forschung und auch keine Missionen":
                None
        },
        inplace=True
    )

    # Mark sub-question as being checked where described aspect fits in one of
    # the existing categories
    data_df = _check_existing_sub_questions(data_df=data_df, question="Q043")

    # Add new categories as additional sub-questions
    data_df = _construct_new_categories(data_df=data_df, question="Q043")

    return data_df


def _prepare_q044(data_df: DataFrame) -> DataFrame:
    """Categorize target platform mentioned in free text field. If target
    platform is mentioned less than 3 times assign "Other" as default
    category."""
    data_df.loc[:, "Q044_other"].replace(
        to_replace={
            "Bleibt in Entwicklngsumgebung (Jupyter Notebooks)":
                "SQ001",
            "Data processing pipeline: there is no interface.":
                "SQ001",
            "Detektor":
                "SQ006",
            "Diese Frage trifft auf mich NICHT zu: "
            "Ich entwickle keine Software - "
            "muss hier allerdings etwas ankreuzen...":
                None,
            "FPGA":
                "SQ006",
            "Gar nichts":
                None,
            "HTC system":
                "SQ005",
            "HTC-System":
                "SQ005",
            "On-site computer cluster":
                "SQ003",
            "Raumfahrtsysteme":
                "SQ006",
            "Roboter":
                "SQ006",
            "VME basierter FPGA-Carrier":
                "SQ006",
            "VME basiertes FPGA-System":
                "SQ006",
            "kein code":
                None,
            "keine Software entwickelt":
                None,
            "nur code":
                "SQ001"
        },
        inplace=True
    )

    # Mark sub-question as being checked where described aspect fits in one of
    # the existing categories
    data_df = _check_existing_sub_questions(data_df=data_df, question="Q044")

    return data_df


def _prepare_q045(data_df: DataFrame) -> DataFrame:
    """Remove free text comment at the end of the survey"""
    data_df.loc[:, "Q045"] = None

    return data_df


def _check_existing_sub_questions(data_df: DataFrame,
                                  question: str) -> DataFrame:
    """Set sub-question value to "Y" where implied in free text answer"""
    sq_id_pattern = r"SQ[0-9][0-9][1-9]|other"
    sub_questions: List = data_df[question + "_other"].dropna().str.findall(
        sq_id_pattern).sum()
    sub_questions = unique(sub_questions)
    for sub_question in sub_questions:
        mask: DataFrame = data_df[question + "_other"] \
            .str.contains(sub_question, na=False)
        data_df.loc[mask, question + "_other"] = None  # remove from Other
        data_df.loc[mask, question + "_" + sub_question] = "Y"  # answer code

    return data_df


def _construct_new_categories(data_df: DataFrame,
                              question: str) -> DataFrame:
    """Add new category where answer is given at least 3 times"""
    # Replace c++ with cpp to avoid re.error: multiple repeat at position 2
    data_df.replace(
        to_replace=r"C.*\+\+.*|c.*\+\+.*",
        value="cpp",
        regex=True,
        inplace=True
    )

    # Disentangle answers of free text field
    free_text_answers: Series = data_df.loc[:, question + "_other"] \
        .dropna() \
        .str.lower() \
        .str.split(r",| and |;| und ") \
        .explode(question + "_other") \
        .str.strip()

    # Unique values
    free_text_answer_options: List = sorted(set(free_text_answers))

    # New categories where answers are mentioned at least 3 times
    new_sub_question_labels: List = [free_text_answer_option for
                                     free_text_answer_option in
                                     free_text_answer_options if
                                     free_text_answer_option != "y" and
                                     free_text_answers.isin(
                                         [free_text_answer_option]).sum() >= 3]

    # Question with sub-questions
    old_sub_question_ids: List = [
        column for column in data_df.columns if column.startswith(question)
    ]

    # Create ids for new categories
    new_sub_question_ids: List = [question + "_SQ" + str(i).zfill(3) for i in
                                  range(len(old_sub_question_ids),
                                        len(old_sub_question_ids) + len(
                                            new_sub_question_labels))]
    new_categories: Dict = dict(zip(new_sub_question_ids,
                                    new_sub_question_labels))
    print("New categories: {}".format(new_categories))

    # Check new sub-question where answer option was given in free text field
    before_other: int = data_df.columns.get_loc(question + "_other")
    for (new_sub_question_id, new_sub_question_label) \
            in new_categories.items():
        # New column for new variable
        data_df.insert(
            loc=before_other, column=new_sub_question_id, value=None
        )
        # Special treatment for "R" and "C" which occur as letters in words
        if len(new_sub_question_label) == 1:
            # find answers
            mask: DataFrame = data_df.loc[:, question + "_other"] \
                .str.contains(
                new_sub_question_label + r"[,;.]\s|" +
                new_sub_question_label + r"[,;. ]|" +
                r"[,;. ]" + new_sub_question_label + r"|" +
                r"[,;.]\s" + new_sub_question_label + r"|" +
                r"^" + new_sub_question_label + r"$",
                regex=True, case=False, na=False
            )
        else:
            # Find answers
            mask: DataFrame = data_df.loc[:, question + "_other"] \
                .str.contains(
                new_sub_question_label, case=False, na=False
            )
        data_df.loc[mask, new_sub_question_id] = "Y"  # answer code
        data_df.loc[mask, question + "_other"] = None  # remove from Other
        before_other = before_other + 1  # move one column further

    return data_df


def _normalize_free_text(data_df: DataFrame, free_text_fields: List) -> str:
    # Apply lemmatizing
    lemmatizer = WordNetLemmatizer()

    # Irrelevant words
    stop_words_de: Set = set(stopwords.words(["german"]))
    stop_words_en: Set = set(stopwords.words(["english"]))
    punctuation: Set = {",", ";", ".", ":", "(", ")", "``", "''", "'", "z.b."}
    stop_words: Set = stop_words_de.union(stop_words_en, punctuation)

    # Combine free text of all fields
    free_text_answers: Series = data_df[free_text_fields] \
        .replace({nan: ''}) \
        .agg(' '.join, axis=1) \
        .dropna() \
        .str.lower() \
        .replace({"-": " ", "/": " ", "n.n.": "", "z.b.": ""}, regex=True) \
        .str.strip()

    # Prepare as text
    free_text: str = " ".join(free_text_answers.values)
    answer_tokens: List = word_tokenize(free_text)

    # Remove stop words
    filtered_answers: List = [word for word in answer_tokens if
                              word not in stop_words]
    print("\nThere are {} words in {} \n".format(len(filtered_answers),
                                                 ", ".join(free_text_fields)))

    normalized_answers: List = []
    for word in filtered_answers:
        word = lemmatizer.lemmatize(word, pos="a")  # adjective
        word = lemmatizer.lemmatize(word)
        normalized_answers.append(word)
    normalized_text: str = \
        " ".join(normalized_answers)  # list to string
    return normalized_text


def _export_data(path: str, data_df: DataFrame) -> None:
    data_df.reset_index(inplace=True)
    data_df.to_csv(
        path_or_buf=path,
        sep=",",
        header=True,
        index=False,
        encoding="utf-8"
    )


data: DataFrame = _load_data(path="./raw-data-hifis-survey-2021.csv")
data = _fix_typos(data_df=data)
data = _remove_invalid_answers(data_df=data)
_prepare_cloud_word_cloud(data_df=data)
data = _prepare_q001(data_df=data)
data = _prepare_q010(data_df=data)
data = _prepare_q011(data_df=data)
data = _prepare_q013(data_df=data)
data = _prepare_q014(data_df=data)
data = _prepare_q021(data_df=data)
data = _prepare_q022(data_df=data)
data = _prepare_q023(data_df=data)
data = _prepare_q024(data_df=data)
data = _prepare_q025(data_df=data)
data = _prepare_q026(data_df=data)
data = _prepare_q030_sq001(data_df=data)
data = _prepare_q030_sq002(data_df=data)
data = _prepare_q030_sq003(data_df=data)
data = _prepare_q030_sq004(data_df=data)
data = _prepare_q030_sq005(data_df=data)
data = _prepare_q031(data_df=data)
data = _prepare_q033(data_df=data)
data = _prepare_q034(data_df=data)
data = _prepare_q037(data_df=data)
data = _prepare_q038(data_df=data)
data = _prepare_q040(data_df=data)
data = _prepare_q043(data_df=data)
data = _prepare_q044(data_df=data)
data = _prepare_q045(data_df=data)
_export_data(path="./data-hifis-survey-2021.csv", data_df=data)
